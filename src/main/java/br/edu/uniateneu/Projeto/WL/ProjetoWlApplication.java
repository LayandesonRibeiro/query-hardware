package br.edu.uniateneu.Projeto.WL;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ProjetoWlApplication {

	public static void main(String[] args) {
		SpringApplication.run(ProjetoWlApplication.class, args);
	}

}
